# this python3 script captures motion detection events using HC501 PIR snsor
# this script is intended to be run as a systemd service
# /lib/systemd/system/capture.service 

from picamera import PiCamera
from gpiozero import MotionSensor
from time import sleep
import datetime
import time
import socket
import fcntl
import struct
import uuid
import os
import _thread
import email, smtplib, ssl
from hashlib import md5
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sqlite3
import shlex
import subprocess
from subprocess import CalledProcessError

# global variables
# schedule[0][1] = 0 means monday 01:00am - 01:59am dont record,
# 1 means do record
schedule = [[1]*24 for i in range(7)] 
# this needs to be set manually, later we make it automatic? 
port = 22
username = "pi"
rsa_private_key = r"/home/pi/.ssh/id_rsa"
dir_local = '/home/pi/'
dir_remote = '/home/pi/'
glob_pattern = '*.*'
local_video_path = "/home/pi/Videos/"
local_picture_path = "/home/pi/Pictures/"
remote_video_path = "/home/pi/Videos/"
remote_picture_path = "/home/pi/Pictures/"
main_server_ip = "raspberrypis.local"
pir = MotionSensor(21)
motion_timeout = 10
video_duration = 5
photo_res = (1280,960)
video_res = (1280,720)
video_framerate = 24
receiver_email = "s.lazarus@student.unsw.edu.au"
detect_boolean = 0   # whether to detect
email_boolean = 0   # whether to send emails

def sendEmail(filename, date):
    subject = "Sensor triggered"
    body = ""
    sender_email = "comp6733asdf@gmail.com"
    global receiver_email
    password = "comp6733uytgb"
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    message["Bcc"] = receiver_email  # Recommended for mass emails
    message.attach(MIMEText(body, "plain"))
    filename = "/home/pi/Pictures/" + date + ".jpg"
    try:
        with open(filename, "rb") as attachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {filename}",
        )
        message.attach(part)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, text)
    except:
        pass

# this gets called when button pressed
# this will get replaced with pir sensor when we get them
def wait(pir):
    global motion_timeout
    pir.wait_for_motion()
    print("Motion detected!")
    global detect_boolean
    if detect_boolean != 1:
        print("Motion detection disabled by remote server")
        return
    # check schedule to see if we should capture
    day = (datetime.datetime.now() + datetime.timedelta(hours=11)).weekday()
    hour = (datetime.datetime.now() + datetime.timedelta(hours=11)).hour
    print("day: " + str(day))
    print("hour: " + str(hour))
    print("schedule[day][hour]: " + str(schedule[day][hour]))
    if schedule[day][hour] == 0:
        print("schedule prevents recording")
        return
    camera.resolution = photo_res
    date = (datetime.datetime.now() + datetime.timedelta(hours=11)).strftime("%Y_%m_%d_%H_%M_%S")
    print("date:" + date)
    picture_file_name = date + ".jpg"
    video_file_name = date + ".h264"
    camera.start_preview()
    picturePath = local_picture_path + picture_file_name
    videoPath = local_video_path + video_file_name
    camera.capture(picturePath)
    print("Captured picture at " + picturePath)
    # send email
    global email_boolean
    if email_boolean == 1:
        sendEmail(date + ".jpg", date)
    # capture the video
    camera.framerate = video_framerate
    camera.resolution = video_res
    camera.start_recording(videoPath)
    sleep(video_duration)
    camera.stop_recording()
    # encode the video
    try:
        output = subprocess.check_output("MP4Box -add " + videoPath + " " + local_video_path+date+".mp4", shell=True) #need sudo apt install gpac
        print("Captured video at " + local_video_path+date+'.mp4')
        os.remove(videoPath)
    except CalledProcessError as e:
        print('FAIL:\ncmd:{}\noutput:{}'.format(e.cmd, e.output))
    camera.stop_preview()
    pir.wait_for_no_motion(timeout=motion_timeout)

def schedule_update():
    while(1):
        sleep(5) # run this every x seconds
        global email_boolean
        global detect_boolean
        global receiver_email
        global photo_res
        global video_res
        global schedule
        try:
            conn = sqlite3.connect('/home/pi/all.db')
            c = conn.cursor()
            c.execute('select * from settings where id=4')
            r = c.fetchall()
            if r[0][1] == "1":
                detect_boolean = 1
            else:
                detect_boolean = 0
            c.execute('select * from settings where id=6') 
            r = c.fetchall()
            receiver_email = r[0][1]
            c.execute('select * from settings where id=7') 
            r = c.fetchall()
            if r[0][1] == "1":
                email_boolean = 1
            else:
                email_boolean = 0
            c.execute('select * from settings where id=10') #thresh std_dev, not used in PIR
            r = c.fetchall()
            thresh_stdev = int(r[0][1])
            c.execute('select * from settings where id=11') #schedule monday
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[0][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=12')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[1][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=13')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[2][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=14')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[3][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=15')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[4][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=16')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[5][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=17')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[6][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=18') #photo
            r = c.fetchall()
            r = int(r[0][1])
            if r == 1:
                photo_res = (1024,768)
            elif r == 2:
                photo_res = (1280,960)
            elif r == 3:
                photo_res = (1600,1200)
            elif r == 4:
                photo_res = (2048,1536)
            elif r == 5:
                photo_res = (2560,1920)
            c = conn.cursor()
            c.execute('select * from settings where id=19') #video
            r = c.fetchall()
            r = int(r[0][1])
            if r == 1:
                video_res = (720,480)
            elif r == 2:
                video_res = (720,576)
            elif r == 3:
                video_res = (1280,720)
            elif r == 4:
                video_res = (1920,1080)
            print("email_boolean: " + str(email_boolean))
            print("detect_boolean: " + str(detect_boolean))
            print("schedule is : ") 
            print(schedule)
            print("receiver email is : " + str(receiver_email)) 
            print("photo_res  is : " + str(photo_res)) 
            print("video_res  is : " + str(video_res)) 
        except:
            print("exception in schedule_update()")

# get our inet ip address
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip_addr = s.getsockname()[0]
s.close()
print(ip_addr)

# get mac address
mac_addr = ':'.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff)for ele in range(0,8*6,8)][::-1])
print(mac_addr)

try:
    _thread.start_new_thread(schedule_update, ())
except:
   print("Error: unable to start thread")
   exit(1)
camera = PiCamera()
while(1):
    wait(pir)
    
    
    
    
    
    

