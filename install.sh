#!/bin/bash

## Check if user is the super user
if [ $UID -ne 0 ]; then
    echo "Superuser privileges are required to run this script."
    echo "Please run: \"sudo $0\""
    exit 1
fi
echo -e "\n\nUpdating Repository Information prepare for install\n\n"
sudo apt-get update

# Create folder link
cd /home/pi
rm -rf /home/pi/Pictures
rm -rf /home/pi/Videos
mkdir /home/pi/Pictures
mkdir /home/pi/Videos
sudo mkdir /var/www
sudo mkdir /var/www/raspberrypis.local/
sudo ln -s /var/www/html /var/www/raspberrypis.local/

echo -e "\n\nInstall Sensors & system environments\n\n"
sudo apt-get -y install python-rpi.gpio python3-rpi.gpio git unzip avahi-daemon debconf-utils
sudo apt install gpac
echo -e "\n\nClone the object from git\n\n"
cd /var/www
rm -rf "comp6733webif"
sudo git clone https://gitlab.cse.unsw.EDU.AU/z5269058/comp6733webif.git

if [ ! -d "comp6733webif" ]; then
   sudo git clone https://gitlab.cse.unsw.EDU.AU/z5269058/comp6733webif.git
fi

if [ ! -d "comp6733webif" ]; then
   sudo git clone https://gitlab.cse.unsw.EDU.AU/z5269058/comp6733webif.git
   if [ ! -d "comp6733webif" ]; then
      echo -e "Git clone 3 times failed. Install terminated."
      exit;
   fi
fi

sudo mv comp6733webif html

echo -e "\n\nWill be the main server?\n('y' for YES, other input for NO)"
read uinput
if [ "$uinput" == "y" ]; then
    echo "PasswordAuthentication yes" | sudo tee -a /etc/ssh/sshd_config
    echo "PermitRootLogin yes" | sudo tee -a /etc/ssh/sshd_config
    sudo sh -c 'echo root:raspberry | chpasswd'
    # Install dependency packages
    echo -e "\n\nInstall PHP Components\n\n"
    sudo apt-get -y install php 
    sudo apt-get -y install php-cgi
    sudo apt-get -y install php-intl
    sudo apt-get -y install php-mbstring
    sudo apt-get -y install php-xml
    sudo apt-get -y install php-common
    sudo apt-get -y install php-sqlite3
    sudo apt-get -y install curl
    sudo apt-get -y install php-cli
    sudo apt-get -y install php-fpm
    sudo apt-get -y install sqlite3
    sudo apt-get -y install libsqlite3-dev
    
    echo -e "\n\nInstalling Composer\n\n"
    curl -sS https://getcomposer.org/installer -o composer-setup.php
    sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    rm -rf composer-setup.php

    echo -e "\n\nInstall Web Server Engine\n\n"
    sudo apt-get -y install apache2
    sudo a2enmod rewrite
    sudo a2enmod proxy_fcgi setenvif
    sudo a2enconf php7.3-fpm
    sudo systemctl restart apache2
    #sudo apt-get -y install nginx

    if [ -d "comp6733webif" ]; then
        sudo rm -rf html
        sudo mv comp6733webif html
        cd /var/www/html
        echo -e "\n\nSetting up default folder link for Photos and Videos\n\n"
        sudo mkdir /var/www/html/webroot/Pictures
        sudo chmod 777 /var/www/html/webroot/Pictures -R
        sudo mkdir /var/www/html/webroot/Videos
        sudo chmod 777 /var/www/html/webroot/Videos -R
        sudo ln -sfn /var/www/html/webroot/Pictures /home/pi
        sudo ln -sfn /var/www/html/webroot/Videos /home/pi
        sudo chown -R pi:pi /home/pi/Pictures
        sudo chown -R pi:pi /home/pi/Videos
        echo -e "\n\nInstall package dependency of server\n\n"
        sudo composer install -n
        cd /var/www/html/config
        sudo cp -rf /var/www/html/config/settings/apache2.conf /etc/apache2/apache2.conf
        echo -e "\n\nRestarting Web Engine\n\n"
        sudo systemctl restart apache2
        #sudo service nginx reload
        echo -e "\n\nConfiguring app.php\n\n"
        sudo rm -rf /var/www/html/config/app.php
        sudo cp /var/www/html/config/settings/app.server.php /var/www/html/config/app.php
        sudo composer install -n
        sudo hostnamectl set-hostname raspberrypis
    fi
    sudo rm -rf /var/www/html/config/app.php
    sudo cp /var/www/html/config/settings/app.server.php /var/www/html/config/app.php
    sudo composer install -n
    sudo hostnamectl set-hostname raspberrypis
else
    echo -e "\n\nInstalling client\n\n"
    echo -e "Setting up ssh key authentication to main server\n"
    echo -e "Just press enter at the prompts, except for when "
    echo -e "connecting to remote server to authenticate key\n\n"
    ssh-keygen
    ssh-keygen -p -m PEM -f /root/.ssh/id_rsa
    ssh-copy-id -i /root/.ssh/id_rsa.pub raspberrypis.local
    echo -e "Installing paramiko\n\n"
    sudo apt-get install python-paramiko -y
    echo -e "Setting up transfer services\n\n"
    sudo cp /var/www/html/config/transfer.py /home/pi/
    sudo cp /var/www/html/config/transfer.service /lib/systemd/system/transfer.service
    sudo systemctl daemon-reload
    sudo systemctl enable transfer.service
    sudo systemctl start transfer.service
fi

echo -e "Update permission request\n\n"
sudo cp -rf /var/www/html/config/settings/sudoers /etc/sudoers
sudo chmod 777 /var/www/html/config
sudo chmod 777 /var/www/html/config/schema
sudo chmod 666 /var/www/html/config/schema/all.db
echo -e "Installing  pycamera\n"
echo -e "Enable it afterwards by running sudo raspi-config\n"
echo -e "(interface options)\n"
sudo apt-get install python3-picamera -y
sudo apt-get install python-picamera -y
sudo apt-get install python3-gpiozero -y
echo -e "Setting up capture services (PIR by default)\n\n"
sudo cp /var/www/html/config/cv_capture.py /home/pi/
sudo cp /var/www/html/config/pir_capture.py /home/pi/
sudo cp /var/www/html/config/pircapture.service /lib/systemd/system/capture.service 
sudo systemctl daemon-reload
sudo systemctl enable capture.service
sudo systemctl start capture.service
sudo chown www-data:www-data /var/www/html -R
sudo chmod 777 /var/www/html/config
sudo chmod 777 /var/www/html/config/schema
sudo chmod 666 /var/www/html/config/all.db
echo -e "You can check on service status by typing\n"
echo -e "sudo systemctl status [capture.service/transfer.service]\n\n"
echo -e "\n\nAll Done ! Yay!\n\n"
echo -e "Dont forget to enable camera by running sudo raspi-config(interface options)\n"
echo -e "Dont forget to enable camera by running sudo raspi-config(interface options)\n"
echo -e "\n\nDo you wish to reboot system now?('y' for Yes, other for NO)\n"
read ureboot
if [ "$ureboot" == "y" ]; then
  sudo reboot
fi
exit 0;
