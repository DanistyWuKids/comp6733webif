<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

/**
 * Recordings Controller
 *
 * @property \App\Model\Table\RecordingsTable $Recordings
 *
 * @method \App\Model\Entity\Recording[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RecordingsController extends AppController
{
    /**
     * All method
     *
     * @param string|null $options Selection of video/photos
     * @return \Cake\Http\Response|null
     */
    public function index($options = null)
    {
        $servername = TableRegistry::getTableLocator()->get('Settings')->get(5)->toArray()['attribute'];
        $photopath = TableRegistry::getTableLocator()->get('Settings')->get(1)->toArray()['attribute'];
        $videopath = TableRegistry::getTableLocator()->get('Settings')->get(2)->toArray()['attribute'];
        $photofolder = new Folder();
        $photofolder->cd(WWW_ROOT.'Pictures');
        $photos = $photofolder->find('.*\.jpg');

        $videofolder = new Folder();
        $videofolder->cd(WWW_ROOT.'Videos');
        $videos = $videofolder->find('.*\.mp4');

        if ($options == null){
            $nums = $this->Recordings->find()->count();
            $recordings = $this->Recordings->find()->toArray();
        } else {
            if ($options == 0) {
                $nums = $this->Recordings->find()->where(['recType' => '0'])->count();
                $recordings = $this->Recordings->find()->where(['recType' => '0'])->toArray();
            } else if ($options == 1) {
                $nums = $this->Recordings->find()->where(['recType' => '1'])->count();
                $recordings = $this->Recordings->find()->where(['recType' => '1'])->toArray();
            }
        }
        $this->set(compact('nums'));
        $this->set(compact('recordings'));
        $this->set(compact('options'));
        $this->set(compact('servername'));
        $this->set(compact('photos'));
        $this->set(compact('videos'));
    }

    /**
     * View method
     *
     * @param string|null $id Recording id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($filename = null)
    {
        if ($filename == null){
            throw new NotAcceptableException();
        }else{
            if (substr($filename,20,3) == "jpg" || substr($filename,20,3) == "JPG"){
                $photofolder = new Folder();
                $photofolder->cd(WWW_ROOT.'Pictures');
                $file = $photofolder->find($filename);
            } else if (substr($filename,20,3) == "mp4" || substr($filename,20,3) == "MP4"){
                $videofolder = new Folder();
                $videofolder->cd(WWW_ROOT.'Videos');
                $file = $videofolder->find($filename);
            } else {
                throw new FileNotFoundException();
            }
        }
        if (count($file) > 0){
            $this->set(compact('file'));
        } else {
            throw new FileNotFoundException();
        }
    }

    public function statics(){

    }

    /**
     * Delete method
     *
     * @param string|null $id Recording id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($filename = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if ($filename == null){
            $this->Flash->error(__('Selected file does not found. Please, try again.'));
            return $this->redirect($this->referer());
        }else{
            if (substr($filename,20,3) == "jpg" || substr($filename,20,3) == "JPG"){
                $photofolder = new Folder();
                $photofolder->cd(WWW_ROOT.'Pictures');
                $file = $photofolder->find($filename);
            } else if (substr($filename,20,3) == "mp4" || substr($filename,20,3) == "MP4"){
                $videofolder = new Folder();
                $videofolder->cd(WWW_ROOT.'Videos');
                $file = $videofolder->find($filename);
            } else {
                $this->Flash->error(__('Selected file does not found. Please, try again.'));
                return $this->redirect($this->referer());
            }
        }
        if (count($file) > 0){
//            $this->set(compact('file'));
            if (substr($filename,20,3) == "jpg" || substr($filename,20,3) == "JPG") {
                echo shell_exec('rm -rf /var/www/html/webroot/Pictures/'.$filename);
            } else if (substr($filename,20,3) == "mp4" || substr($filename,20,3) == "MP4"){
                echo shell_exec('rm -rf /var/www/html/webroot/Videos.'.$filename);
            }
            $this->Flash->success(__('The recording has been deleted.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Selected file does not found. Please, try again.'));
            return $this->redirect($this->referer());
        }
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow();
    }
}
