<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recording $recording
 */

use Cake\I18n\Time;

$this->assign('title', 'View Recording');
use Cake\ORM\TableRegistry;
?>
<div class="container-fluid">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a><?= $this->Html->link('Dashboard', ['controller' => 'Pages', 'action' => 'index']) ?></a>
        </li>
        <li class="breadcrumb-item">Recordings</li>
        <li class="breadcrumb-item active">View Recordings</li>
    </ol>

    <h1 class="h3 mb-2 text-gray-800"><?= __('View Recording') ?></h1>
    <p class="mb-4"><br>
        Your recording information will be shown here
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Recordings</h6>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <td>File was captured on:</td>
                    <?php $time = Time::create(substr($file[0],0,4),substr($file[0],5,2),substr($file[0],8,2),
                        substr($file[0],11,2), substr($file[0],14,2),substr($file[0],17,2))?>
                    <td><?php echo $time->i18nFormat('dd-MM-yyyy HH:mm:ss')?></td>
                </tr>
                </thead>
            </table>
            <?php
                if (substr($file[0],20,3)=="jpg"||substr($file[0],20,3)=="JPG"){
                    echo $this->Html->image("/Pictures/".$file[0],['width'=>'100%']);
                } else if (substr($file[0],20,3)=="mp4"||substr($file[0],20,3)=="MP4"){?>
                    <video width="100%" controls>
                        <source src="<?php echo $this->Url->build('/Videos/'.$file[0],true)?>" type="video/mp4">
                        Sorry, seems you  browser don't support with embedded view of videos.
                    </video>
                <?php } ?>
        </div>
    </div>
</div>
