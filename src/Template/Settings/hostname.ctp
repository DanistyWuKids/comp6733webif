<?php
$this->assign('title','Set Storage Path');

echo $this->Html->css("/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.css");
echo $this->Html->script('/vendor/sbadmin2/datatables/jquery.dataTables.min.js');
echo $this->Html->script('/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.js');
echo $this->Html->script('sbadmin2/demo/datatables-demo.js');
?>
<div class="container-fluid">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a><?= $this->Html->link('Dashboard', ['controller' => 'Pages', 'action' => 'index']) ?></a>
        </li>
        <li class="breadcrumb-item">Settings</li>
        <li class="breadcrumb-item active">Hostname</li>
    </ol>

    <h1 class="h3 mb-2 text-gray-800"><?= __('Set hostname') ?></h1>
    <p class="mb-4"><br>
        You can modify your hostname at here.
        <br><br>
        Once submitted, it will take some time applied to all devices.
    </p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit Hostname</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <p>Note: hostname are case sensitive.</p>
                <?= $this->Form->create() ?>
                <?php echo $this->Form->control('hostname',['label'=>'Hostname:','value'=>$hostname->attribute,'class'=>'form-control','required'=>'true']);?>
                <br><br>
                <?= $this->Form->button('Submit',['class'=>'btn btn-primary']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
