<?php
$this->assign('title','Set Storage Path');

echo $this->Html->css("/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.css");
echo $this->Html->script('/vendor/sbadmin2/datatables/jquery.dataTables.min.js');
echo $this->Html->script('/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.js');
echo $this->Html->script('sbadmin2/demo/datatables-demo.js');
?>
<div class="container-fluid">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a><?= $this->Html->link('Dashboard', ['controller' => 'Pages', 'action' => 'index']) ?></a>
        </li>
        <li class="breadcrumb-item">Settings</li>
    </ol>

    <h1 class="h3 mb-2 text-gray-800"><?= __('Settings') ?></h1>
    <p class="mb-4"><br>
        Select an action to continue <br><br>
    </p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Actions list</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <?php echo $this->Html->link('Set working schedules', ['controller' => 'settings', 'action' => 'workingtime']) ?>
                <br><br>
                <?php echo $this->Html->link('Change storage path', ['controller' => 'settings', 'action' => 'editpath']); ?>
                <br><br>
                <?php echo $this->Html->link('Set Server hostname', ['controller' => 'settings', 'action' => 'hostname']); ?>
                <br><br>
                <?php echo $this->Html->link('Email Alert settings', ['controller' => 'settings', 'action' => 'email']); ?>
<!--                <br><br>-->
<!--                --><?php //echo $this->Html->link('Cloud Storage settings', ['controller' => 'settings', 'action' => 'cloud']);?>
                <br><br>
                <?php echo $this->Html->link('Camera Settings',['controller'=>'settings','action'=>'camera'])?>
            </div>
        </div>
    </div>
</div>
