# COMP6733 Surveillance Project Web interface

## END-OF-LIFE Support Notice (27 November 2019):
This project is build by Oh-My-IoT group for UNSW 2019 Term 3 unit COMP6733.

Project Contributor:

Script Implements & Testings:

    Daigo Conlan, Sam Lazarus

Website interface:

    Wenkai Gong

The project final report has also been added to this repository.

---

Below is the Readme for this project:

## Project info

This is a web interface application build with [CakePHP](https://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

### Easy 3 step install:

1. Check latest [script](https://gist.githubusercontent.com/DanistyWuKids/00b168a1a7903f39c1f16b375c4e80de ) and download it.

Or download the script by:
```bash
$ wget https://gist.githubusercontent.com/DanistyWuKids/00b168a1a7903f39c1f16b375c4e80de/raw/029bc1fdb10b9e59ab7bfbed88f1f4440420d5e0/install.sh
```

2. Adding executable permission to script.
```bash
$ chmod +x install.sh
```

3. Execute Install script by (Require Superuser privileges):

```bash
$ sudo ./install.sh
```

### The step-by-step method:
1. Install following packages on raspberry pi.

```bash
$ sudo apt-get install -y php php-cgi php-intl php-mbstring php-xml php-xmlrpc php-common curl php-cli php-fpm apache2 libapache2-mod-php
$ sudo apt-get install -y php-sqlite3 sqlite3
$ sudo apt-get install -y python-rpi.gpio python3-rpi.gpio git unzip avahi-daemon debconf-utils
```

2. After installation, configure the ***/etc/apache2/apache2.conf*** on section ***/var/www*** as follow shows.

```ini
<Directory /var/www/>
	Options Indexes FollowSymLinks
	AllowOverride All
	Require all granted
</Directory>
```

3. Download and install the [Composer](https://getcomposer.org/doc/00-intro.md) by following procedures

```bash
$ curl -sS https://getcomposer.org/installer -o composer-setup.php
$ sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```


4. Clone this project to Apache webroot folder, default location is "/var/www/".

```bash
$ cd /var/www
$ sudo git clone https://gitlab.cse.unsw.edu.au/z5269058/comp6733webif
```
If this is your first time create a website, then follow the below commands, otherwise, please follow this [tutorial](https://www.liquidweb.com/kb/configure-apache-virtual-hosts-ubuntu-18-04/).
```bash
$ sudo rm -rf html
$ sudo mv comp6733webif html
```

5. Create a soft link to user home directory for picture and video folders

```bash
$ sudo ln -sfn /var/www/html/webroot/Pictures /home/pi
$ sudo ln -sfn /var/www/html/webroot/Videos /home/pi
```

6. Change the owner of directory to Web engine account 
``` bash
$ sudo chown www-data:www-data /var/www/ -R
```

7. Execute following commands within the project folder

```bash
$ sudo a2enmod rewrite
$ sudo a2enmod proxy_fcgi setenvif
$ sudo systemctl restart apache2
```

8. Check your php-fpm version and enable it in apache 2

```bash
$ sudo a2enconf php<your version>-fpm
$ sudo systemctl restart apache2
```

9. Modify your sudoers file to allow web engine to shutdown/reboot the system. By adding following lines to sudoers file.

```ini
%www-data ALL=NOPASSWD: /sbin/shutdown
%www-data ALL=NOPASSWD: /sbin/reboot
```

10. You can now either use your Apache Web Engine by browsing `http://localhost`, or start up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Configuration

NB: If you are installed by using script and successfully installed all require components. No further configures required.

Read and edit `config/app.php` and setup the `'Datasources'` and any other configuration relevant for your application.

## Default Credentials of website

Username: admin

Password: admin

## Layout

This application is using open ourced bootstrap theme sb-admin2 as management template. Original Template can be found on [GitHub](https://github.com/BlackrockDigital/startbootstrap-sb-admin-2)
